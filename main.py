from math import exp

import matplotlib.pyplot as plt

import CavityRadiation as cavity



# Constants for calculations
# sigma units = W * m^-2 * K^-4
SIG = 5.67e-8
# Wein's displacement constant, units = micro-m * K
B = 2.898e-3
# Planck's constant, units = J * s
H = 6.62618e-34
# speed of light, unit = m * s^-1
C = 2.9979e8
# Boltzmann's constant, units = J * K^-1
K = 1.38066e-23

cav1 = cavity.CavityRadiation(273)

min_wavelength = 1e-9
max_wavelength = cav1.peak_wavelength()

temperature = 273
wavelength = 1e-9

print((H * C) / (wavelength * K * temperature))

# num = exp((H * C) / (wavelength * K * temperature))

# wavelengths, power_spectrum = cav1.power_spectrum(min_wavelength, max_wavelength)

# plt.figure(1)
# plt.scatter(wavelengths, power_spectrum)
# plt.xlabel('Wavelength')
# plt.ylabel('Power')
# plt.xlim(0, max_wavelength)
# plt.ylim(0, max(power_spectrum))

# plt.show()

# for i in range(len(wavelengths)):
#     print("Wavelengths:", wavelengths[i], "Power:", power_spectrum[i])
