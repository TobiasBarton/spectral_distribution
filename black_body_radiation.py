from math import pow, exp
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


class BlackBody:
    # sigma units = W * m^-2 * K^-4
    # should be a static variable
    sig = 5.67e-8
    # Wein's displacement constant, units = micro-m * K
    b = 2.898e-3
    # Planck's constant, units = J * s
    h = 6.62618e-34
    # speed of light, unit = m * s^-1
    c = 2.9979e8
    # Boltzmann's constant, units = J * K^-1
    k = 1.38066e-23
    power_spectrum = []
    list_wavelength = []

    def __init__(self, temperature, min_range=0, max_range=0):
        # each class will have it's own temperature
        self.temperature = temperature
        self.min_range = min_range
        self.max_range = max_range

    def total_emissive_power(self):
        # gives the total emissive power for a absolute temperature
        # stores the total emissive power in 'power'
        total_power = self.sig * (self.temperature ** 4)
        # returns the value for future use
        return total_power

    def wavelength_max(self):
        # determines the max wavelength for a absolute temperature
        # temperature should be in kelvin and wavelength is in meters
        # stores the max wavelength in 'wave_max'
        wave_max = (self.b / self.temperature)
        # returns the value for future use
        return wave_max

    def emissive_power(self, wavelength):
        # gives the emissive power for a given wavelength and temperature
        # units of temperature should be kelvin and wavelength meters
        exp_part = (self.h * self.c) / (wavelength * self.k * self.temperature)
        power = ((2 * self.h * pow(self.c, 2)) / pow(wavelength, 5)) * (1 / (exp(exp_part) - 1))
        # units of emissive power are kg m^-1 s^-1
        return power

    def wavelength_range(self):
        middle_range = self.wavelength_max()
        self.min_range = middle_range / 10
        self.max_range = middle_range * 10

    def get_min_range(self):
        return self.min_range

    def get_max_range(self):
        return self.max_range

    def emissive_power_spectrum(self):
        # calculates the emissive power for a spectrum
        self.wavelength_range()
        for i in range(0, 1000):
            wavelength_tmp = random.uniform(self.min_range, self.max_range)
            self.list_wavelength.append(wavelength_tmp)
            self.power_spectrum.append(self.emissive_power(wavelength_tmp))
#        spectrum_df = pd.DataFrame(self.power_spectrum, index=self.list_wavelength)
        spectra = {'Emissive_power': pd.Series(self.power_spectrum),
                   'Wavelength': pd.Series(self.list_wavelength)}
        return pd.DataFrame(spectra)
#        spectrum_df.plot(kind='scatter', x='Wavelength', y='Emissive power')
#        plt.show()
