from math import pow, exp
import random
import numpy as np
import matplotlib.pyplot as plt


class CavityRadiation:
    # Constants for calculations
    # sigma units = W * m^-2 * K^-4
    SIG = 5.67e-8
    # Wein's displacement constant, units = micro-m * K
    B = 2.898e-3
    # Planck's constant, units = J * s
    H = 6.62618e-34
    # speed of light, unit = m * s^-1
    C = 2.9979e8
    # Boltzmann's constant, units = J * K^-1
    K = 1.38066e-23

    def __init__(self, temperature):
        """
        The temperature of the black body cavity must be in Kelvin
        """
        self.temperature = temperature

    def total_emissive_power(self):
        """
        Gets the total power of a black body cavity for the temperature of the cavity
        :return: Total power
        """
        return self.SIG * (self.temperature ** 4)

    def peak_wavelength(self):
        """
        The wavelength with the highest energy output from a temperature
        :return: The peak wavelength in m
        """
        return self.B / self.temperature

    def emissive_power(self, wavelength):
        """
        The emissive power of the black body cavity for a given wavelength
        :return: Power
        """
        print("Wavelength:", wavelength)
        exp_part = (self.H * self.C) / (wavelength * self.K * self.temperature)
        if exp_part > 250:
            # If exp_part is large then 
            power = 0
        else:
            power = ((2 * self.H * pow(self.C, 2)) / pow(wavelength, 5)) * (1 / (exp(exp_part) - 1))
        return power

    def power_spectrum(self, min_wavelength, max_wavelength, sample_size=1000):
        """
        Gets the power of the cavity over a range of wavelengths
        """
        # min_wavelength =< samples < max_wavelength
        wavelengths = np.random.uniform(min_wavelength, max_wavelength, sample_size)
        vfunc = np.vectorize(self.emissive_power, otypes=[np.float])
        powers = vfunc(wavelengths)
        return wavelengths, powers


if __name__ == "__main__":
    cavity = CavityRadiation(300)
    print("Peak wavelength", cavity.peak_wavelength())
    wavelengths, powers = cavity.power_spectrum(100e-9, 600e-9)
    plt.figure(1)
    plt.scatter(wavelengths, powers)
    plt.xlabel('Wavelength')
    plt.ylabel('Power')
    plt.show()
